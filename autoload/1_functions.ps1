Function global:Home {
	cd ${env:HOME}
}

Function global:Sudo([string]$file, [string[]]$args) {
	if([System.IO.File]::Exists("$(get-location)\$file")) {
		$file = "$(Get-Location)\$file";
	}
	$psi = new-object System.Diagnostics.ProcessStartInfo $file;
	$psi.Arguments = $args;
	$psi.Verb = "runas";
	[System.Diagnostics.Process]::Start($psi);
}

# From: http://stackoverflow.com/questions/9999963/powershell-test-admin-rights-within-powershell-script
Function global:Test-IsAdmin {
    try {
        $identity = [Security.Principal.WindowsIdentity]::GetCurrent()
        $principal = New-Object Security.Principal.WindowsPrincipal -ArgumentList $identity
        return $principal.IsInRole( [Security.Principal.WindowsBuiltInRole]::Administrator )
    } catch {
        throw "Failed to determine if the current user has elevated privileges. The error was: '{0}'." -f $_
    }

    <#
        .SYNOPSIS
            Checks if the current Powershell instance is running with elevated privileges or not.
        .EXAMPLE
            PS C:\> Test-IsAdmin
        .OUTPUTS
            System.Boolean
                True if the current Powershell is elevated, false if not.
    #>
}

Function global:l([string]$path = ".") {
  gci -Path $path | fw -AutoSize
}

Function global:la([string]$path = ".") {
  gci -Attributes d,a,r,h,s
}

Function global:..() {
    cd ..
}

Function global:clear2 {
  clear
  motd
}

if(Test-Path alias:\cd) {
    Remove-Item alias:\cd
}

Function global:cd([string]$path) {
  Set-Location $path
  if(Test-Path ".local.ps1") {
    iex .local.ps1
  }
}

Function global:ql {
	$args
}

Function devilx {
    ssh mat3u@devilx -p 9022
}

Function global:vienv {
    [CmdletBinding()]
    param (
        [string]$Script
    )

    vim "$($env:TOOLS)\autoload\$($Script)"
}

$global:options['CustomArgumentCompleters']['vienv:Script'] = {
    param($commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameter)

        ls $env:TOOLS\autoload -Filter "$($wordToComplete)*.ps1" | sort -Property Name | % {
            New-Object System.Management.Automation.CompletionResult $_.Name, $_.Name, 'ParameterValue', $_.Name
        }
}

Function global:cdenv {
    cd $env:TOOLS
}

Function global:envdebug {
    param([switch]$setDebug)

    if($setDebug -eq $True) {
        $env:ENVDEBUG=$True
    } else {
        Remove-Item env:ENVDEBUG -ErrorAction SilentlyContinue
    }
}

set-alias Env-Vi vienv -Scope Global
set-alias Env-Debug envdebug -Scope Global
set-alias Env-GoToRoot cdenv -Scope Global

Function global:Env-ReloadPath {
    $env:Path = [System.Environment]::GetEnvironmentVariable("Path","Machine") + ";" + [System.Environment]::GetEnvironmentVariable("Path","User")
}

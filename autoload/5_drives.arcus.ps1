if($null -eq (Get-PSDrive PhD -ErrorAction SilentlyContinue)) {
    New-PSDrive -Name PhD -PSProvider FileSystem -Root P:\PhD -Scope Global | Out-Null
}

if($null -eq (Get-PSDrive OS -ErrorAction SilentlyContinue)) {
    New-PSDrive -Name OS -PSProvider FileSystem -Root P:\OpenSource -Scope Global -ErrorAction SilentlyContinue | Out-Null
}

if($null -eq (Get-PSDrive Blog -ErrorAction SilentlyContinue)) {
    New-PSDrive -Name Blog -PSProvider FileSystem -Root P:\Blog -Scope Global -ErrorAction SilentlyContinue | Out-Null
}

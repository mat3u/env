$global:envs = @{
    "py27" = "c:\Users\Matt\Anaconda3\envs\py27;c:\Users\Matt\Anaconda3\envs\py27\Scripts";
    "py33" = "c:\Users\Matt\Anaconda3\envs\py33;c:\Users\Matt\Anaconda3\envs\py33\Scripts";
    "py34" = "c:\Users\Matt\Anaconda3;c:\Users\Matt\Anaconda3\Scripts";
    "haskell" = "c:\Program Files (x86)\Haskell Platform\2013.2.0.0\bin\;C:\Users\Matt\AppData\Roaming\cabal\bin;C:\Program Files (x86)\Haskell Platform\2013.2.0.0\lib\extralibs\bin;C:\Program Files (x86)\Haskell Platform\2013.2.0.0\mingw\bin";
    "erlang" = "c:\Program Files\erl6.0\bin\;S:\Dev\rebar\";
    "node" = "C:\Program Files\nodejs\;C:\Users\Matt\AppData\Roaming\npm;.\node_modules\.bin";
    "latex" = "C:\Program Files (x86)\MiKTeX 2.9\miktex\bin;";
    "fsharp" = "C:\Program Files (x86)\Microsoft SDKs\F#\3.1\Framework\v4.0\";
    "ruby" = "C:\Ruby193\bin"
}

function global:Env-Register {
    [cmdletbinding()]
    param()
    $env:ENV_PATH = ""

    Env-GetEnabledEnvs | ? { $_ -in $envs.Keys } | % {
        Write-Verbose ("Registering `"{0}`"" -f $_)

        $env:ENV_PATH += ";" + $envs[$_]
    }

    # Share ENVVARS between sessions
    [System.Environment]::SetEnvironmentVariable("ENV_PATH", $env:ENV_PATH, "User")

    Env-ReloadPath
}

function global:Env-GetEnabledEnvs {
    if(-not $env:ENV_ENABLED) {
        return
    }

    $env:ENV_ENABLED.Split(';') | ? { $_.Trim() -ne "" } | % {
        Write-Output $_
    }
}

function global:Env-SetEnabledEnvs {
    param(
        [string[]] $enabled_envs = @()
    )

    $env:ENV_ENABLED = ($enabled_envs -join ";")

    Env-Register
}

function global:Env-Reset {
    Env-SetEnabledEnvs
}

function global:Env-Enable {
    [CmdletBinding()]
    param(
       [Parameter(Mandatory=$true)]
       [string] $Name
    )

    if($Name -notin $envs.Keys) {
        Write-Error "Unknown environment: $($Name)!"
        return
    }

    $active_envs = Env-GetEnabledEnvs

    if($active_envs -contains $Name) {
        Write-Host -Fore Magenta ("Environment '{0}' was already active!" -f ($Name))
        return
    }

    if($null -eq $active_envs) {
        Env-SetEnabledEnvs $Name
    } else {
        Env-SetEnabledEnvs $active_envs,$Name
    }
    Write-Host -Fore Green ("Environment '{0}' activated!" -f ($Name))
}

function global:Env-Disable {
    [CmdletBinding()]
    param(
       [Parameter(Mandatory=$true)]
       [string] $Name
    )

    if($Name -notin $envs.Keys) {
        Write-Error "Unknown environment: $($Name)!"
    }

    $env_to_be_saved = Env-GetEnabledEnvs | ? { $_ -ne $Name }

    if($null -eq $env_to_be_saved) {
        Env-Reset
    } else {
        Env-SetEnabledEnvs $env_to_be_saved
    }

    Write-Host -Fore Green ("Environment '{0}' deactivated!" -f ($Name))
}

function global:Env-Status {
    $env_enabled = Env-GetEnabledEnvs

    $envs.Keys | % {
        $o = New-Object PSObject

        $o | Add-Member "Environment Name" $_
        $o | Add-Member "Active" ($_ -in $env_enabled)
        $o | Add-Member "Path" $envs[$_]

        Write-Output $o
    }
}

$global:options['CustomArgumentCompleters']['Env-Enable:Name'] = {
    param($commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameter)

    $env_enabled = Env-GetEnabledEnvs

    $envs.Keys | ? { $_.StartsWith($wordToComplete) } | % {
        if($_ -in $env_enabled) {
            return
        }

        New-Object System.Management.Automation.CompletionResult $_, $_, 'ParameterValue', $_
    }
}

$global:options['CustomArgumentCompleters']['Env-Disable:Name'] = {
    param($commandName, $parameterName, $wordToComplete, $commandAst, $fakeBoundParameter)

    $env_enabled = Env-GetEnabledEnvs

    $envs.Keys | ? { $_.StartsWith($wordToComplete) } | % {
        if($_ -notin $env_enabled) {
            return
        }

        New-Object System.Management.Automation.CompletionResult $_, $_, 'ParameterValue', $_
    }
}

Env-Register

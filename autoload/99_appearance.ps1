# Appearance

function global:prompt {
    $realLASTEXITCODE = $LASTEXITCODE

    #Write-VcsStatus

    Write-GitStatus (Get-GitStatus)

    if(Test-IsAdmin) {
      Write-Host -NoNewLine " $([char]0x03BB)" -ForegroundColor Red
    } else {
      Write-Host -NoNewLine " $([char]0x03BB)" -ForegroundColor DarkCyan
    }

    $title = "PS | $env:computername | $pwd"

    $host.UI.RawUI.WindowTitle =  $title

    $global:LASTEXITCODE = $realLASTEXITCODE
    " "
}

Enable-GitColors

Pop-Location

Start-SshAgent -Quiet

# Done
# MOTD Definition
Env-ReloadPath

Function global:Get-Memory {
    $sysInfo = Get-WmiObject -Class Win32_OperatingSystem -Namespace root/cimv2 -ComputerName .

    $freeMem =  [Math]::Round($sysInfo.FreePhysicalMemory / (1kB))
    $totalMem = [Math]::Round($sysInfo.TotalVisibleMemorySize / (1kB))

    return $freeMem, $totalMem
}

Function global:Get-Uptime {
    $bootdate = (Get-CimInstance -ClassName win32_operatingsystem).lastbootuptime
    $uptime = (get-date) - $bootdate

    return $uptime
}

Function global:motd {
    $motd = gc ($env:TOOLS + "\misc\motd2")

    pushd
    cd $env:TOOLS
    $version = (git rev-parse HEAD).Substring(0, 9)

    $motd = $motd.Replace('%VERSION%', $version)

    $motd
    popd

    Write-Host -Fore Blue (" "*6) -NoNewLine

    $freeMem, $totalMem = Get-Memory
    $uptime = Get-Uptime

    $uptimeS = $uptime.ToString("d'd:'hh':'mm':'ss")

    Write-Host -NoNewLine -Fore Cyan "Mem: "
    Write-Host -NoNewLine -Fore Green ("{0}MB" -f $freeMem)
    Write-Host -NoNewLine "/"
    Write-Host -NoNewLine -Fore Red ("{0}MB" -f $totalMem)
    Write-Host -NoNewLine -Fore Cyan " Uptime: "
    Write-Host -NoNewLine -Fore Green $uptimeS

    Write-Host
    Write-Host
}

if(-not $env:ENVDEBUG) {
	clear
}

motd

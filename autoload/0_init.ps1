$env:PSModulePath += ";$($env:TOOLS)\misc\WindowsPowerShell\Modules"

# Tab Expansion
if (-not $global:options) { $global:options = @{CustomArgumentCompleters = @{};NativeArgumentCompleters = @{}}}
$function:tabexpansion2 = $function:tabexpansion2 -replace 'End\r\n{','End { if ($null -ne $options) { $options += $global:options} else {$options = $global:options}'

# Import modules
if ($host.Name -eq 'ConsoleHost')
{
    Import-Module PSReadline
    Import-Module posh-git
    Import-Module pswatch
}

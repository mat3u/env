Set-Alias vim "$(${ENV:ProgramFiles(x86)})\Vim\vim74\vim.exe" -Scope Global
Set-Alias gvim "$(${ENV:ProgramFiles(x86)})\Vim\vim74\gvim.exe" -Scope Global

set-alias csc C:\Windows\Microsoft.NET\Framework\v4.0.30319\csc.exe -scope Global
Set-Alias msbuild C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe -Scope Global

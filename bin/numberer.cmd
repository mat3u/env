@echo off

reg Query "HKLM\Hardware\Description\System\CentralProcessor\0" | find /i "x86" > NUL && set OS=32BIT || set OS=64BIT

if %OS%==32BIT "32/7+ Taskbar Numberer.exe"
if %OS%==64BIT "64/7+ Taskbar Numberer.exe"

params(
    [string]$f
)

latex -c-style-errors -interaction=nonstopmode -quiet "$($f).tex"
latex -c-style-errors -interaction=nonstopmode -quiet "$($f).tex"
latex -c-style-errors -interaction=nonstopmode -quiet "$($f).tex"
bibtex $f
bibtex $f

latex -c-style-errors -interaction=nonstopmode -quiet "$($f).tex"
latex -c-style-errors -interaction=nonstopmode -quiet "$($f).tex"

dvips "$($f).dvi"
rm "$($f).dvi"
ps2pdf "$($f).ps"
rm "$($f).ps"

# Save current directory position
pushd

# Looking for TOOLS dir
$path = pwd

while(!(Test-Path $path\.git -Type container)) {
	$tmp = pwd
	cd ..
	if($path -eq $tmp){
		Write-Host -Fore Red "ERROR: Unable to find TOOLS root directory!"
		exit
	}

	$path = $tmp
}

if(Test-Path $path\misc\WindowsPowerShell\mat3u.env -Type Leaf) {
	Write-Host -Fore Red "ERROR: Earlier installation detected!"
	exit
}

Write-Host -Fore Red "This installation may harm your computer!"

Write-Host $path -Fore Blue

$userinput = Read-Host "Is this your TOOLS dir (y/N)"

$path = ($path).ToString()

if($userinput.toLower() -eq "y"){ 
	Write-Host -Fore Cyan "This may take several minutes..."

	Write-Progress -activity "Configuring environment..." -status "" -PercentComplete 0

	# Setup environment variables

	[System.Environment]::SetEnvironmentVariable("TOOLS", $path, "User")

	$up = [System.Environment]::GetEnvironmentVariable("PATH", "User")
	$up = $up + ";" + $path + "\bin;" + $path + "\usr\bin"
	$up = [System.Environment]::SetEnvironmentVariable("PATH", $up, "User")

	# Setup PowerShell Profile

	rm $HOME\Documents\WindowsPowerShell -Force -ErrorAction SilentlyContinue

	iex "$path\bin\junction.exe" "$HOME\Documents\WindowsPowerShell" "$path\misc\WindowsPowerShell"

	Write-Progress -activity "Configuring environment..." -status "Installing: PowerShell Here" -PercentComplete 5

	# Install 'PowerShell Prompt Here'
	rundll32 syssetup,SetupInfObjectInstallAction DefaultInstall 128 $path\init\powershellhere.inf

	Write-Progress -activity "Configuring environment..." -status "Installing: Chocolatey" -PercentComplete 10

	# Vim

	Set-Content -Value "set runtimepath=$($path)/misc/Vim/vimfiles,$VIMRUNTIME`n`rsource $($path)/misc/Vim/_vimrc" -Path "$($HOME)/_vimrc"

	# Install Chocolayey
	iex ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
	cmd /C "SET PATH=%PATH%;%systemdrive%\chocolatey\bin"

	cinst toolsroot 

	Write-Progress -activity "Configuring environment..." -status "Installing packages - High" -PercentComplete 15

	# Install software - High
	cinst git
	cinst SublimeText2.app 
	cinst autohotkey_l.install

		# Auto Hotkey to Startup
		# TODO: Refactor - extract method for future use

		$WshShell = New-Object -comObject WScript.Shell
		$Shortcut = $WshShell.CreateShortcut("$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Startup\AHK.lnk")
		$Shortcut.TargetPath = "C:\Program Files\AutoHotkey\AutoHotkeyU64.exe"
		$Shortcut.Arguments = "$path\misc\AutoHotkey.ahk"
		$Shortcut.Save()

	Write-Progress -activity "Configuring environment..." -status "Installing packages - Medium" -PercentComplete 25

	# Install software - Medium
	cinst truecrypt
	#cinst nodejs.install
	cinst sysinternals
	cinst vagrant 
	cinst ilspy 

	Write-Progress -activity "Configuring environment..." -status "Installing packages - Low" -PercentComplete 50

	# Install software - Low
	cinst Firefox 
	cinst flashplayerplugin 
	cinst skype
	cinst calibre
	cinst gimp
	cinst adobereader 
	cinst winmerge 
	cinst foobar2000 

	Write-Progress -activity "Configuring environment..." -status "Done!" -PercentComplete 100

	# Other

	Write-Host -Fore Green "Applications needs to be installed manually: 
		`n`t* Visual Studio
		`n`t* Office 2013
		`n`t* Multi Commander
		`n`t* K-Lite Codec Pack
		`n`t* LaTeX
		`n`t* WTW"
}

# Go to the init directory
popd
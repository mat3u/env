param(
	[string]$EnvPath
)

pushd

Write-Host "Setup environment variables"

[System.Environment]::SetEnvironmentVariable("TOOLS", $EnvPath, "User")

$up = [System.Environment]::GetEnvironmentVariable("PATH", "User")
$up = $up + ";" + $EnvPath + "\bin;"
$up = [System.Environment]::SetEnvironmentVariable("PATH", $up, "User")

Write-Host "Setup PowerShell Profile"

rm $HOME\Documents\WindowsPowerShell -ErrorAction SilentlyContinue
iex "cmd /c mklink /J $HOME\Documents\WindowsPowerShell $EnvPath\misc\WindowsPowerShell"

Write-Host "Make link to ssh keys"

iex "cmd /c mklink /J $HOME\.ssh $EnvPath\misc\ssh-keys"

Set-Content -Value ("set runtimepath=$($EnvPath)/misc/Vim/viminfo,$($EnvPath)/misc/Vim/viminfo/autoload,`$VIMRUNTIME`n`rsource $($EnvPath)/misc/Vim/.vimrc" -replace "\\","/") -Path "$($HOME)/_vimrc"

Write-Host "Enable AHK Shortcut"

rm "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Startup\AHK.lnk" -Force -ErrorAction SilentlyContinue

$WshShell = New-Object -comObject WScript.Shell
$Shortcut = $WshShell.CreateShortcut("$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Startup\AHK.lnk")
$Shortcut.TargetPath = "C:\Program Files\AutoHotkey\AutoHotkeyU64.exe"
$Shortcut.Arguments = "$EnvPath\misc\AutoHotkey.ahk"
$Shortcut.Save()

Write-Host "Install Powershell Modules"

(new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex

Install-Module Posh-Git
Install-Module PsReadline
Install-Module Posh-npm
Install-Module pswatch

popd

; #=Win ^=Ctrl +=Shift !=Alt

; PowerShell

^`::
IfWinExist ahk_class VirtualConsoleClass
{
	IfWinActive ahk_class VirtualConsoleClass
	{
		WinMinimize
	}
	else
	{
		WinActivate
	}
}
else
{
	;SetWorkingDir %USERPROFILE%
	SetWorkingDir P:
	Run "S:\Dev\Cmder\Cmder.exe"
}
return

#IfWinActive ahk_class ConsoleWindowClass
^V::
SendInput {Raw}%clipboard%
return
#IfWinActive

; MSTSC - Remote Desktop

; Virtual Machine

^!q::
	Run mstsc.exe
return

; vm-mstasch.fp.lan
!q::
IfWinExist ahk_class TscShellContainerClass
	WinActivate
else
	Run mstsc.exe /v:fp-pc2687.fp.lan /f /multimon
return

^Capslock:: 
  Sleep 50
  WinMinimize A    ; need A to specify Active window
  WinActivate ahk_class Progman
return

#t:: ; <win+t> to change transparency
DetectHiddenWindows, on
WinGet, curtrans, Transparent, A
if ! curtrans
    curtrans = 255
newtrans := curtrans - 32
if newtrans > 0
{
    WinSet, Transparent, %newtrans%, A
}
else
{
    WinSet, Transparent, 255, A
    WinSet, Transparent, OFF, A
}
return

#o:: ; <win+o> to reset transparency
WinSet, Transparent, 255, A
WinSet, Transparent, OFF, A
return
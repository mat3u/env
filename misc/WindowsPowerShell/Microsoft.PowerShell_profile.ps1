﻿# Do not edit this file!
# Put any changes to autoload scripts!

# Autoload
Function EnvUp($tools = "${env:TOOLS}") {
    foreach($file in (Get-ChildItem "$tools\autoload\*.ps1" | Sort-Object $_.Name))
    {
        $isMachineBound = $file.Name.IndexOf(".") -ne $file.Name.LastIndexOf(".");

        if($isMachineBound)
        {
            $machine = $file.BaseName.Substring($file.Name.IndexOf(".") + 1).ToUpper()

            if($machine -ne [System.Environment]::MachineName.ToUpper())
            {
                continue;
            }
        }

        echo "LOAD: $($file.Name)"
        & $file.FullName
    }
}

EnvUp
